# INF4375 Projet de Session

Auteur: Michael Tessier

Code Permanent: TESM17069209

## Points Développées
------
### A1

Le script s'attend à se connecter à mongoDB via le port par défaut 27017.

Pour exécuter le script (dans le dossier principal du projet): 

`node downloadScript.js`

La base de données TESM17069209 est créée avec la collection ActesCriminels.

### A2

Le module est dans `downloadData.js`.
La 'cron job' est initié dans `app.js`.

### A3

Pour démarrer le serveur (dans le dossier principal du projet):

`npm start`

### A4

Pour tester la fonctionnalité, alors que le serveur est en marche, visitez:

[http://localhost:3000/actes?date=2015-03-12](http://localhost:3000/actes?date=2015-03-12)

Vous pouvez changer la date dans l'URL pour visionner différentes données.

### A5

Pour tester la fonctionnalité, alors que le serveur est en marche, visitez:

[http://localhost:3000/actes/mensueljson](http://localhost:3000/actes/mensueljson)

### A6

Pour tester la fonctionnalité, alors que le serveur est en marche, visitez:

[http://localhost:3000/actes/mensuelxml](http://localhost:3000/actes/mensuelxml)

### C1

Pour tester la fonctionnalité, alors que le serveur est en marche, visitez:

[http://http://localhost:3000/actes/distance?latitude=45.5372886&longitude=-73.5904142](http://localhost:3000/actes/distance?latitude=45.5372886&longitude=-73.5904142)

Note: Il n'y a présentement aucune valeur affichée car le contenu le plus récent est plus vieux que 3 mois. 

### D1

L'information additionel est enregistré dans la collection LieuxCulturels de la même base de données.

### D2

Pour tester la fonctionnalité, alors que le serveur est en marche, visitez:

[http://localhost:3000/lieux](http://localhost:3000/lieux)

### F1