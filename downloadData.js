const request = require('request-promise');
const iconv = require('iconv');
const csv = require('csvtojson');
const mongoClient = require('mongodb').MongoClient;

module.exports = function() {
    const url = 'mongodb://localhost:27017/';

    mongoClient.connect(url, (err, db) => {
        if (err) throw err;
        var dbo = db.db("TESM17069209")
        insertActesCriminels(dbo, () => {
            insertLieuxCulturels(dbo, () => {
                db.close();
                console.log('Task Completed.');
            });
        });
    });

    const insertActesCriminels = (dbo, callback) => {
        try{
            let collection = dbo.collection('ActesCriminels');
            collection.remove((err, succ) => {
                if (err) throw err;
                if (succ) {
                    const csvURL = 'http://donnees.ville.montreal.qc.ca/dataset/5829b5b0-ea6f-476f-be94-bc2b8797769a/resource/c6f482bf-bf0f-4960-8b2f-9982c211addd/download/interventionscitoyendo.csv';
                    request.get({
                        uri: csvURL,
                        encoding: null})
                        .then((csvData) => {
                            return csv().fromString(toUTF8(csvData));
                        })
                        .then((jsonArray) => {
                                collection.insert(jsonArray, function(err, res) {
                                    if (err) throw err;
                                    callback();
                                });
                        });
                }
            });
        }
        catch(err) {
            console.log(err);
            callback();
        }
    }

    const insertLieuxCulturels = (dbo, callback) => {
        try{
            let collection = dbo.collection('LieuxCulturels');
            collection.remove((err, succ) => {
                if (err) throw err;
                if (succ) {
                    const csvURL = 'http://donnees.ville.montreal.qc.ca/dataset/ceb2427e-aa50-4d06-b13a-d1b21e2702b9/resource/974ca8d4-ed85-4c77-a79f-9f765e4c1b32/download/lieuxculturels.csv';
                    request.get({
                        uri: csvURL,
                        encoding: null})
                        .then((csvData) => {
                            return csv().fromString(csvData.toString());
                        })
                        .then((jsonArray) => {
                                collection.insert(jsonArray, function(err, res) {
                                    if (err) throw err;
                                    callback();
                                });
                        });
                }
            });
        }
        catch(err) {
            console.log(err);
            callback();
        }
    }
}

function toUTF8(body) {
    var ic = new iconv.Iconv('latin1', 'utf-8');
    var buf = ic.convert(body);
    return buf.toString('utf-8');
  }