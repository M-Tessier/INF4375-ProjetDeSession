var mongoose = require('mongoose');

var acteCriminelSchema = mongoose.Schema({
  Categorie: { type:String, required: true },
  Date: { type:String, required: true },
  Quart: { type:String, required: true },
  PDQ: { type:String, required: true },
  X: { type:String, required: true },
  Y: { type:String, required: true },
  LAT: { type:String, required: true },
  LONG: { type:String, required: true }
});

var ActesCriminels = module.exports = mongoose.model('ActesCriminels', acteCriminelSchema);

module.exports.getActesCriminelsByDate = function(date, callback) {
  ActesCriminels.find(callback);
}