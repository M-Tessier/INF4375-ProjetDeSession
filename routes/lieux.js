var express = require('express');
var haversine = require('haversine');

const mongoClient = require('mongodb').MongoClient;

var router = express.Router();

/* GET actes listing. */
router.get('/', function(req, res, next) {
  const url = 'mongodb://localhost:27017/';

  mongoClient.connect(url, (err, db) => {
    try{
      if (err) throw err;
      var dbo = db.db("TESM17069209")
      let collectionActes = dbo.collection('ActesCriminels');
      let collectionLieux = dbo.collection('LieuxCulturels');
      collectionLieux.find().toArray((err, lieux) => {
        if(err) throw err;
        collectionActes.find().toArray((err, actes) => {
            if(err) throw err;
            res.json(haversineMatch(lieux, actes));
        });
      });
    }
    catch(err) {
      db.close();
    }
  });
});


module.exports = router;

function toMensuel(docs) {
  var foundMonth = false;
  var foundType = false;
  var array = [];
  docs.forEach((value)=> {
    var date = value.DATE.substring(0, 7);
    var type = value.CATEGORIE;
    array.forEach((month) => {
      if(month.mois == date) { 
        foundMonth = true;
        month.actes.forEach((nb) => {
          if(nb.type == type) {
            foundType = true;
            nb.nombre++;
          }
        });
        if(!foundType) {
          var nouvelActe = { type: type, nombre: 1};
          month.actes.push(nouvelActe);
        }
      }
      foundType = false;
    })
    if(!foundMonth) {
      var object = { mois: date, actes: [] };
      var acte = {type: type, nombre: 1};
      object.actes.push(acte);
      array.push(object);
    }
    foundMonth = false;

  });
  return array;
}

function haversineMatch(lieux, actes) {
  var foundLieu = false;
  var array = [];
  lieux.forEach((lieu) => {
    var lieuLAT = parseFloat(lieu.Latitude);
    var lieuLONG = parseFloat(lieu.Longitude);
    var a = {
      latitude: lieuLAT,
      longitude: lieuLONG
    };
    //console.log('a' + a);
    actes.forEach((acte) => {
        var acteLAT = parseFloat(acte.LAT);
        var acteLONG = parseFloat(acte.LONG);
        var b = {
        latitude: acteLAT,
        longitude: acteLONG
        };
        //console.log('b' + b);
        //console.log(haversine(a, b));
        if(haversine(a, b) < 1 && acte.DATE.substring(0, 4) == "2018") {
            array.forEach((value) => {
                if(value.Description == lieu.Description) {
                    foundLieu = true;
                    value.Crimes++;
                }
            });
            if(!foundLieu) {
                lieu.Crimes = 1;
                array.push(lieu);
            }
        }
        foundLieu = false;
    });
  });
  return array;
}