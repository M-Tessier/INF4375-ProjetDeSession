var express = require('express');
var js2xmlparser = require('js2xmlparser');
var haversine = require('haversine');

const mongoClient = require('mongodb').MongoClient;

var router = express.Router();

/* GET actes listing. */
router.get('/', function(req, res, next) {
  const url = 'mongodb://localhost:27017/';

  mongoClient.connect(url, (err, db) => {
    try{
      if (err) throw err;
      var dbo = db.db("TESM17069209")
      let collection = dbo.collection('ActesCriminels');
      collection.find( { DATE: req.query.date}).toArray((err, docs) => {
        if(err) throw err;
        res.json(docs);
        db.close();
        });
    }
    catch(err) {
      db.close();
    }
  });
});

router.get('/mensueljson', function(req, res, next) {
  const url = 'mongodb://localhost:27017/';

  mongoClient.connect(url, (err, db) => {
    try{
      if (err) throw err;
      var dbo = db.db("TESM17069209")
      let collection = dbo.collection('ActesCriminels');
      collection.find().toArray((err, docs) => {
        if(err) throw err;
        res.send(toMensuel(docs));
        db.close();
      });
    }
    catch(err) {
      db.close();
    }
  });
});

router.get('/mensuelxml', function(req, res, next) {
  const url = 'mongodb://localhost:27017/';

  mongoClient.connect(url, (err, db) => {
    try{
      if (err) throw err;
      var dbo = db.db("TESM17069209")
      let collection = dbo.collection('ActesCriminels');
      collection.find().toArray((err, docs) => {
        if(err) throw err;
        res.set('Content-Type', 'text/xml');
        //var x = js2xmlparser.parse(toMensuel(docs)[0], {compact: true, ignoreComment: true, spaces: 4});
        res.send(js2xmlparser.parse("ActesMensuel", toMensuel(docs)));
        db.close();
      });
    }
    catch(err) {
      db.close();
    }
  });
});

router.get('/distance', function(req, res, next) {
  const url = 'mongodb://localhost:27017/';

  mongoClient.connect(url, (err, db) => {
    try{
      if (err) throw err;
      var dbo = db.db("TESM17069209")
      let collection = dbo.collection('ActesCriminels');
      collection.find().toArray((err, docs) => {
        if(err) throw err;
        res.json(haversineMatch(docs, req.query.latitude, req.query.longitude));
        db.close();
        });
    }
    catch(err) {
      db.close();
    }
  });
});

module.exports = router;

function toMensuel(docs) {
  var foundMonth = false;
  var foundType = false;
  var array = [];
  docs.forEach((value)=> {
    var date = value.DATE.substring(0, 7);
    var type = value.CATEGORIE;
    array.forEach((month) => {
      if(month.mois == date) { 
        foundMonth = true;
        month.actes.forEach((nb) => {
          if(nb.type == type) {
            foundType = true;
            nb.nombre++;
          }
        });
        if(!foundType) {
          var nouvelActe = { type: type, nombre: 1};
          month.actes.push(nouvelActe);
        }
      }
      foundType = false;
    })
    if(!foundMonth) {
      var object = { mois: date, actes: [] };
      var acte = {type: type, nombre: 1};
      object.actes.push(acte);
      array.push(object);
    }
    foundMonth = false;

  });
  return array;
}

function haversineMatch(docs, alat, along) {
  var array = [];
  docs.forEach((object) => {
    var blat = parseFloat(object.LAT);
    var blong = parseFloat(object.LONG);
    var a = {
      latitude: alat,
      longitude: along
    };
    var b = {
      latitude: blat,
      longitude: blong
    };
    var aDate = new Date();
    var bDate = new Date(object.DATE);
    if(haversine(a, b) < 3 && (((aDate - bDate)/(1000*60*60)) <= 2190)) {
      array.push(object);
    }
  });
  return array;
}